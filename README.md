Simple Web Starter
==================

Your code can be put in the `static` directory.

Running
-------

To run the project you need to run `npm install` in the project folder followed by `nodejs server.js` or `forever start server.js` if using forever.

Terms of Use
------------

This project is provided as is. The author accepts no responsibility for any harm that may come through any form from using this project. You should use this at your own discretion.