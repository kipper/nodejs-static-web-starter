var express = require("express");
var logger = require("morgan");
var fs = require("fs");
var http = require("http");
var path = require("path");
var bodyparser = require("body-parser");

var PORT = 5995;
var app = express();
var accessLog = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})
var staticPath = path.resolve(__dirname, "static");

app.use(bodyparser.json());
app.use(logger("short", {stream: accessLog}));
app.use(express.static(staticPath));
app.get("/", function(req, res){
    res.sendFile(path.resolve(staticPath, "index.html"));
});

http.createServer(app).listen(PORT);

console.log("Listenning on port: " + PORT);